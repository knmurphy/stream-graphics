function create_edit_form(path) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/search_prefix/' + path, true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            data = JSON.parse(this.response);

            var table_body = document.getElementById('edit-table-body');

            Object.keys(data).sort().forEach(function (value) {

                var row = table_body.insertRow(-1);

                var key_cell = row.insertCell(0);
                var value_cell = row.insertCell(1);
                var submit_cell = row.insertCell(2);

                var key_label = document.createElement('label');
                key_label.setAttribute('for', 'data-' + value + '-input');
                key_label.innerText = value;

                key_cell.appendChild(key_label);

                var value_input = document.createElement('input');
                value_input.setAttribute('class', 'form-control');
                value_input.classList.add('form-control');
                value_input.type = 'text';
                value_input.id = 'data-' + value + '-input';
                value_input.value = data[value];

                value_cell.appendChild(value_input);

                var submit_button = document.createElement('button');
                submit_button.type = 'button';
                submit_button.classList.add('btn');
                submit_button.classList.add('btn-primary');
                submit_button.innerText = 'Send';
                submit_button.onclick = function () {
                    data_send(value)
                };

                submit_cell.appendChild(submit_button);
            });

            if (window.console) console.log(data);
        } else {
            if (window.console) console.log("Error downloading init data.");

        }
    };

    request.send();
}

function data_send(path) {
    var value = document.getElementById('data-' + path + '-input').value;
    if (window.console) console.log('Submitting data for', path, "-", value);

    var data_to_send = {};
    data_to_send[path] = value;

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/set_multiple", true);
    xhttp.send(JSON.stringify(data_to_send));
}

function data_form_get(path) {
    return document.getElementById('data-' + path + '-input').value;
}

function data_form_change(path, value) {
    return document.getElementById('data-' + path + '-input').value = value;
}

function raw_send(path, value) {
    if (window.console) console.log('Submitting data for', path, "-", value);

    var data_to_send = {};
    data_to_send[path] = value;
    data_form_change(path, value);

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/set_multiple", true);
    xhttp.send(JSON.stringify(data_to_send));
}

function restore_default(path) {
    if (window.console) console.log('Restoring', path);

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/restore_default/" + path, true);
    xhttp.send();
}

function restore(path, value) {
    if (window.console) console.log('Restoring', path, "-", value);

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/restore/" + path + "/" + value, true);
    xhttp.send();
}

function reload_restore_properties() {
    if (window.console) console.log('Reloading Restore List');

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/reload_restore_properties", true);
    xhttp.send();
}

function run_script(path) {
    if (window.console) console.log('Running', path);

    var data_to_send = {};

    for (let parameter of document.querySelectorAll("[data-script='" + path + "']")) {
        console.log(parameter.dataset.parameter ,parameter.value);
        data_to_send[parameter.dataset.parameter] = parameter.value
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/run_data_source/" + path, true);
    xhttp.send(JSON.stringify(data_to_send));
}

function stop_script(path) {
    if (window.console) console.log('Stopping', path);

    var xhttp = new XMLHttpRequest();
    xhttp.open("DELETE", "/api/v1/run_data_source/" + path, true);
    xhttp.send();
}

function form_data_update(path) {

    element = document.getElementById('data-' + path + '-input');
    if (element) {
        element.value = data[path];
    }
}
