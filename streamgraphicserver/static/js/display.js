function premix_init(premix_name) {
    if (window.console) console.log('Loading premix for:', premix_name);

    // Should not be needed as it should already exist, but just in case.
    if (!document.getElementById('premix/' + premix_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'premix/' + premix_name;
        new_elem.classList.add('premix');

        document.body.appendChild(new_elem);
    }

    if (data.hasOwnProperty('premix/' + premix_name + '/overlays')) {
        data['premix/' + premix_name + '/overlays'].split(',').forEach(function (overlay_name) {
            overlay_init(premix_name, overlay_name);
        });
    }
}

function overlay_init(premix_name, overlay_name) {
    if (window.console) console.log('Loading overlay for:', overlay_name);

    if (!document.getElementById('overlay/' + overlay_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'overlay/' + overlay_name;
        new_elem.classList.add('overlay');

        document.getElementById('premix/' + premix_name).appendChild(new_elem);

    }

    if (data.hasOwnProperty('overlay/' + overlay_name + '/components')) {
        data['overlay/' + overlay_name + '/components'].split(',').forEach(function (component_name) {
            component_init(overlay_name, component_name);
        });
    }
    if (data.hasOwnProperty('overlay/' + overlay_name + '/display/' + mode)) {
        var overlay_div = document.getElementById('overlay/' + overlay_name);

        overlay_div.classList.forEach(function (value) {
            if (value.startsWith('display-' + mode)) {
                overlay_div.classList.remove(value);
            }
        });

        overlay_div.classList.add('display-' + data['overlay/' + overlay_name + '/display/' + mode]);
    }

}

function manual_overlay_init(div_name, overlay_name) {
    if (window.console) console.log('Loading overlay for:', overlay_name);

    if (!document.getElementById('overlay/' + overlay_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'overlay/' + overlay_name;
        new_elem.classList.add('overlay');

        document.getElementById(div_name).appendChild(new_elem);
    }

    if (data.hasOwnProperty('overlay/' + overlay_name + '/components')) {
        data['overlay/' + overlay_name + '/components'].split(',').forEach(function (component_name) {
            component_init(overlay_name, component_name);
        });
    }
    if (data.hasOwnProperty('overlay/' + overlay_name + '/display-' + mode)) {
        var overlay_div = document.getElementById('overlay/' + overlay_name);

        overlay_div.classList.forEach(function (value) {
            if (window.console) console.log('class: ', value);
            if (value.startsWith('display-' + mode)) {
                overlay_div.classList.remove(value);
            }
        });

        overlay_div.classList.add('display-' + data['overlay/' + overlay_name + '/display/' + mode]);
    }
}

function component_init(overlay_name, component_name) {
    if (window.console) console.log('Loading component for:', component_name);

    if (!document.getElementById('component/' + component_name)) {
        var new_elem = document.createElement('div');

        new_elem.id = 'component/' + component_name;
        get_all_component_styles(component_name, mode).forEach(function (style) {
            new_elem.classList.add(style.split('/')[4] + '-' + data[style])
        });

        document.getElementById('overlay/' + overlay_name).appendChild(new_elem);

        if (data.hasOwnProperty('component/' + component_name + '/mode')) {
            new_elem.setAttribute('data-mode', data['component/' + component_name + '/mode']);
        } else {
            new_elem.setAttribute('data-mode', 'raw');
        }

        if (data.hasOwnProperty('component/' + component_name + '/custom-style')) {
            new_elem.setAttribute('style', data['data/' + data['component/' + component_name + '/custom-style']]);
            new_elem.setAttribute('custom-style', 'data/' + data['component/' + component_name + '/custom-style']);
        }

        var inner_div = document.createElement('div');

        inner_div.classList.add('inner-component');
        new_elem.appendChild(inner_div);

        if (data.hasOwnProperty('component/' + component_name + '/data')) {
            new_elem.setAttribute('data-name', 'data/' + data['component/' + component_name + '/data']);
            data_init(component_name, data['component/' + component_name + '/data'])
        }
    }
}

function data_init(component_name, data_name) {
    if (window.console) console.log('Loading data for:', data_name);

    display_data_update('data/' + data_name);
}

function display_premix_update(path) {
    var split_path = path.split('/');

    if (split_path[2] === 'overlays') {
        document.getElementById('premix/' + split_path[1]).innerHTML = '';
        data[path].split(',').forEach(function (value) {
            overlay_init(split_path[1], value);
        });
    }
}

function display_overlay_update(path) {
    var split_path = path.split('/');
    if (split_path[2] === 'components') {
        document.getElementById('overlay/' + split_path[1]).innerHTML = '';
        data[path].split(',').forEach(function (value) {
            component_init(split_path[1], value);
        })
    } else if (split_path[2] === 'display' && split_path[3] === mode) {
        var overlay_div = document.getElementById('overlay/' + split_path[1]);

        overlay_div.classList.forEach(function (value) {
            if (value.startsWith('display')) {
                overlay_div.classList.remove(value);
            }
        });

        overlay_div.classList.add('display-' + data[path]);
    }
}

function display_component_update(path) {
    var split_path = path.split('/');
    var elem = document.getElementById('component/' + split_path[1]);

    if (split_path[2] === 'style') {

        Array.from(elem.classList).forEach(function(class_item) {
            if (class_item.startsWith(split_path[4]) && (split_path[3] === mode || split_path[3] === 'default')) {
                elem.classList.remove(class_item);
                elem.classList.add(split_path[4] + '-' + data[path]);
            }
        });
    } else if (split_path[2] === 'data') {
        elem.getElementsByClassName('inner-component')[0].innerHTML = '';
        elem.setAttribute('data-name', 'data/' + data[path]);
        data_init(split_path[1], data[path]);
    } else if (split_path[2] === 'mode') {
        if (elem) {
            elem.setAttribute('data-mode', data[path]);
        }
        if (elem.hasAttribute('data-name')) {
            var data_path = elem.getAttribute('data-name');
            elem.getElementsByClassName('inner-component')[0].innerHTML = '';
            display_data_update(data_path);
        }
    } else if (split_path[2] === 'custom-style') {
        if (elem) {
            elem.setAttribute('style', data['data/' + data[path]]);
            elem.setAttribute('custom-style', 'data/' + data[path]);
        }
    }
}

function display_data_update(path) {
    if (!(path in data)) {
        return;
    }

    document.querySelectorAll("[custom-style='" + path + "']").forEach(function (element) {
        element.style = data[path];
    });

    document.querySelectorAll("[data-name='" + path + "']").forEach(function (element) {
        var inner_element = element.getElementsByClassName('inner-component')[0];

        if (element.hasAttribute('data-mode')) {
            var mode = element.getAttribute('data-mode');

            if (mode === 'raw') {
                inner_element.innerHTML = data[path];
            } else if (mode === 'qr-code') {
                console.log('adding a QR code');
                while (inner_element.hasChildNodes()) {
                    inner_element.removeChild(inner_element.firstChild);
                }

                new QRCode(inner_element, {
                    text: data[path],
                    width: 512,
                    height: 512,
                    border: 2
                });
            } else if (mode === 'round') {
                console.log('rounding a number to 2 decimals');
                inner_element.innerHTML = Number(data[path]).toFixed(2);
            } else if (mode === 'image') {
                console.log('adding an image');
                inner_element.innerHTML = '<img src=\'' + data[path] + '\'/>';
            } else if (mode === 'svg') {
                console.log('adding an image');
                inner_element.innerHTML = '<img src=\'' + data[path] + '\'/>';
                SVGInject(inner_element.getElementsByTagName('img')[0]);
            } else if (mode === 'series-score') {
                console.log('adding a series score');
                inner_element.innerHTML = '<i class=\'fas fa-circle\'></i>'.repeat(parseInt(data[path], 10));
            } else if (mode === 'font-awesome') {
                console.log('adding a font awesome icon');
                inner_element.innerHTML = '<i class=\'' + data[path] + '\'></i>';
            } else if (mode === 'prefix-dollar-sign') {
                console.log('adding a dollar sign prefix');
                inner_element.innerHTML = '<i class="fas fa-dollar-sign"></i> ' + data[path];
            } else if (mode === 'minecraft-status-icon') {
                console.log('adding a Minecraft status icon');
                if (data[path] !== '') {
                    inner_element.innerHTML =
                        '<img src=\'/static/local/gaming-icons/minecraft/health-formated/' + data[path] + '.png\' style=\'image-rendering: pixelated;\'/>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'minecraft-inventory-icon') {
                console.log('adding a Minecraft inventory icon');
                if (data[path] !== '') {
                    inner_element.innerHTML =
                        '<img src=\'/static/local/gaming-icons/minecraft/inv/' + data[path] + '.png\' style=\'image-rendering: pixelated;\'/>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'minecraft-effect-icons') {
                console.log('adding a Minecraft effect icons');
                if (data[path] !== '') {
                    inner_element.innerHTML = '';
                    var inner_span = '<span style="border-radius: .25em; background-color: #b2b2b2; padding-right: .25em;">';


                    data[path].split(',').forEach(function (effect) {
                        var image = effect.split('|')[0];
                        var time = effect.split('|')[1];

                        inner_span +=
                            '<img src=\'/static/local/gaming-icons/minecraft/effects/' + image + '.png\' style=\'image-rendering: pixelated; height: 50%; margin-left: .25em;\'/> ' + time;
                    });

                    inner_element.innerHTML += inner_span + '</span>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'csgo-icon') {
                console.log('adding a CS:GO icon');
                if (data[path] !== '') {
                    inner_element.innerHTML =
                        '<img src=\'/static/local/gaming-icons/csgo/weapon_icons/' + data[path] + '.png\'/>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'hltv-csgo-icon') {
                console.log('adding a CS:GO icon');
                if (data[path] !== '') {
                    inner_element.innerHTML =
                        '<img src=\'/static/local/gaming-icons/csgo/weapon_icons/weapon_' + data[path] + '_active.png\'/>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'lol-champion') {
                console.log('adding a LoL Champion icon');
                if (data[path] !== '') {
                    inner_element.innerHTML =
                        '<img src=\'https://ddragon.leagueoflegends.com/cdn/10.7.1/img/champion/' + data[path] + '.png\'/>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'csgo-round-kills-icon') {
                console.log('adding a CS:GO round kills icon');
                if (parseInt(data[path]) > 1) {
                    inner_element.innerHTML = '<span class="fa-layers fa-fw">' +
                        '<i class="fas fa-skull" style="transform: scale(.75);"></i>' +
                        '<span class="fa-layers-counter" style="transform: scale(.35);">' + data[path] + '</span>' +
                        '</span>';
                } else if (parseInt(data[path]) > 0) {
                    inner_element.innerHTML = '<span class="fa-layers fa-fw">' +
                        '<i class="fas fa-skull" style="transform: scale(.75);"></i>' +
                        '</span>';
                } else {
                    inner_element.innerHTML = '';
                }
            } else if (mode === 'length-bar-100') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var new_elem = document.createElement('div');

                    new_elem.id = path;
                    new_elem.classList.add('overlay');
                    new_elem.style.backgroundColor = "black";
                    new_elem.style.transition = "all 1s linear";
                    new_elem.style.height = "100%";

                    inner_element.appendChild(new_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else if (mode === 'health-bar-100') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#800000";
                    health_elem.style.transition = "all 1s linear";
                    health_elem.style.height = "100%";
                    health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else if (mode === 'csgo-ct-health-bar-100') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#5D79AE";
                    health_elem.style.transition = "all .5s linear";
                    health_elem.style.height = "100%";
                    health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else if (mode === 'csgo-t-health-bar-100') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#B37D2A";
                    health_elem.style.transition = "all .5s linear";
                    health_elem.style.height = "100%";
                    health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else if (mode === 'rl-boost-total-home') {
                console.log('adding a percent based length bar - rl home');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#002cbf";
                    health_elem.style.transition = "all .5s linear";
                    health_elem.style.height = "100%";
                    //health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = parseFloat(data[path]) * 100 + '%';
            } else if (mode === 'rl-boost-total-away') {
                console.log('adding a percent based length bar - rl away');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#b23e00";
                    health_elem.style.transition = "all .5s linear";
                    health_elem.style.height = "100%";
                    //health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = parseFloat(data[path]) * 100 + '%';
            } else if (mode === 'rl-boost-total') {
                console.log('adding a percent based length bar - rl away');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#00802d";
                    //health_elem.style.transition = "all .25s linear";
                    health_elem.style.height = "100%";
                    //health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = parseFloat(data[path]) * 100 + '%';
            } else if (mode === 'csgo-ct-health-bar-100-no-animate') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#5D79AE";
                    health_elem.style.height = "100%";
                    health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else if (mode === 'csgo-t-health-bar-100-no-animate') {
                console.log('adding a percent based length bar');
                if (!document.getElementById(path)) {
                    var health_elem = document.createElement('div');

                    health_elem.id = path;
                    health_elem.classList.add('overlay');
                    health_elem.style.backgroundColor = "#B37D2A";
                    health_elem.style.height = "100%";
                    health_elem.style.transform = "skewX(-10deg)";

                    inner_element.appendChild(health_elem);
                }

                document.getElementById(path).style.width = data[path] + '%';
            } else {
                console.log('Warning: Unknown mode for ', element.id);
                inner_element.innerHTML = data[path];
            }
        } else {
            inner_element.innerHTML = data[path];
        }
    });
}
