function create_list(mode) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/' + mode + '_list', true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var list_data = JSON.parse(this.response);

            if (window.console) console.log(list_data);

            manual_create_list(mode, list_data.sort());
        } else {
            if (window.console) console.log("Error downloading init data.");

        }
    };

    request.send();
}

function manual_create_list(mode, list_data) {
    list_data.forEach(function (value) {
        document.getElementById(mode + '-list').innerHTML +=
            '<div class="card">' +
        /*    '    <div class="embed-responsive embed-responsive-16by9 card-img-top">\n' +
            '        <div id="' + mode + '/' + value + '" class="' + mode + ' embed-responsive-item"></div>\n' +
            '    </div>\n' +*/
            '    <div class="card-body">\n' +
            '        <h5 class="card-title">' +
            '            <a href="/16x9/' + mode + '/' + value + '">' + value + '</a>' +
            '        </h5>\n' +
            '    </div>\n' +
            '</div>';

        /* if (mode === 'premix') {
            premix_init(value);
        } else if (mode === 'overlay') {
            manual_overlay_init(mode + '/' + value, value);
        } else {
            if (window.console) console.log('Something went wrong... Mode=' + mode + ' is unrecognized');
        } */
    });
}
