SVGInject.setOptions({
    makeIdsUnique: false,
    afterInject: function(img, svg) {
      svg.style.width = "auto";
      svg.style.height = "auto";
    }
});

var socket = io.connect('/websocket');

socket.on('connect', function () {
    console.info('connect');
});
socket.on('reconnect', function (attemptNumber) {
    console.info('reconnect');
});
socket.on('connect_timeout', function (timeout) {
    console.info('connect_timeout');
});
socket.on('reconnect_error', function (error) {
    console.info('reconnect_error');
});
socket.on('connect_error', function (error) {
    console.info('connect_error');
});
socket.on('error', function (error) {
    console.info('error');
});
socket.on('reconnect_failed', function () {
    console.info('reconnect_failed');
});
socket.on('disconnect', function (reason) {
    console.info('disconnect');
});

socket.on('update', function (msg) {
    //console.info('update ' + msg['path'] + ': ' + msg['data']);
    on_update(msg['path'], msg['data']);
});

var data = {};

var div_height = 0;

function get_all_data(callback) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/export', true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            data = JSON.parse(this.response);
            console.log(data);
            callback();
        } else {
            console.log("Error downloading init data.");

        }
    };

    request.send();
}

function get_all_component_styles(component_name, mode) {
    return Object.keys(data).filter(function (propertyName) {
        return (propertyName.indexOf('component/' + component_name + "/style/" + mode) === 0 || propertyName.indexOf('component/' + component_name + "/style/default") === 0 );
    });
}

function on_update(path, value) {
    if (data[path] !== value) {
        data[path] = value;

        //console.log('Update:', path, value);

        var split_path = path.split('/');


        if (split_path[0] === 'data') {
            if (typeof display_data_update === "function") {
                display_data_update(path);
            }
            if (typeof form_data_update === "function") {
                form_data_update(path);
            }
        } else if (split_path[0] === 'premix') {
            if (document.getElementById('premix/' + split_path[1])) {
                if (typeof display_premix_update === "function") {
                    display_premix_update(path);
                }
                if (typeof form_premix_update === "function") {
                    form_premix_update(path);
                }
            }
        } else if (split_path[0] === 'overlay') {
            if (document.getElementById('overlay/' + split_path[1])) {
                if (typeof display_overlay_update === "function") {
                    display_overlay_update(path);
                }
                if (typeof form_overlay_update === "function") {
                    form_overlay_update(path);
                }
            }
        } else if (split_path[0] === 'component') {
            if (document.getElementById('component/' + split_path[1])) {
                if (typeof display_component_update === "function") {
                    display_component_update(path);
                }
                if (typeof form_component_update === "function") {
                    form_component_update(path);
                }
            }
        }
    } else {
        //console.log(path, '- no update -', value);
    }
}
