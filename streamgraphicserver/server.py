import logging
import multiprocessing
import os
from threading import Thread

import redis
import sass
from flask import Flask, render_template, jsonify, request
from flask_executor import Executor
from flask_socketio import SocketIO

from streamgraphicserver.data_sources.data_sources import (
    run_data_source_in_background,
    stop_data_source,
    get_data_source_list_formatted,
    get_data_source_list,
)
from streamgraphicserver.data_sources.net.counter_strike.game_state import (
    parse_csgo_data,
)
from streamgraphicserver.util.data import (
    REDIS_CONNECTION,
    REDIS_HOST,
    delete_keys,
    get_data,
    get_keys,
    send_data,
    send_data_multiple,
    send_event_log,
)
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.restore import (
    get_restore_properties_cache,
    list_restore_file_names,
    list_restore_properties,
    run_script,
)

APP_VERSION = "9.0.0"

app = Flask(__name__)
executor = Executor(app)

multiprocessing.set_start_method("spawn")

setup_loggers()

logging.captureWarnings(True)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

werkzeug_log = logging.getLogger("werkzeug")
werkzeug_log.setLevel(logging.INFO)

engineio_logger = logging.getLogger("engineio_logger")
engineio_logger.setLevel(logging.WARNING)

sio = SocketIO(
    app, async_mode="gevent", message_queue=REDIS_HOST, engineio_logger=engineio_logger,
)


@app.route("/")
def serve_index():
    return render_template("index.jinja2")


@app.route("/premix_list")
def serve_premix_list():
    return render_template("premix_list.jinja2")


@app.route("/restore_list")
def serve_restore_list():
    return render_template(
        "restore_list.jinja2", restore_properties=get_restore_properties_cache()
    )


@app.route("/script_list")
def serve_script_list():
    return render_template(
        "script_list.jinja2", script_properties=get_data_source_list()
    )


@app.route("/<mode>/overlay/<overlay_name>")
def serve_overlay(mode, overlay_name):
    return render_template(
        "overlay.jinja2", mode=mode, overlay_name=overlay_name, version=APP_VERSION
    )


@app.route("/<mode>/event_log/<log_name>")
def serve_event_log(mode, log_name):
    return render_template(
        "event_log.jinja2", mode=mode, log_name=log_name, version=APP_VERSION
    )


@app.route("/utility/<mode>/grid")
def serve_utility_grid(mode):
    return render_template(
        "grid.jinja2", mode=mode, overlay_name="grid", version=APP_VERSION
    )


@app.route("/<mode>/premix/<premix_name>")
def serve_premix(mode, premix_name):
    return render_template(
        "premix.jinja2", mode=mode, premix_name=premix_name, version=APP_VERSION
    )


@app.route("/edit/<path:path>")
def serve_data(path):
    return render_template("edit.jinja2", path=path)


@app.route("/editor_template/<template_name>")
def serve_editor_template(template_name):
    return render_template("editor_templates/" + template_name + ".jinja2")


@app.route("/api/v1/event_log/<log_name>", methods=["POST"])
def serve_api_event_log_display(log_name):
    text_right = request.args.get("text_right", default="", type=str)
    text_left = request.args.get("text_left", default="", type=str)
    image = request.args.get("image", default="", type=str)
    fa_icon = request.args.get("fa_icon", default="", type=str)
    separator = request.args.get("separator", default=": ", type=str)
    bg_color = request.args.get("bg_color", default="black", type=str)
    text_color = request.args.get("text_color", default="white", type=str)
    time = request.args.get("time", default=5000, type=int)

    send_event_log(
        log_name=log_name,
        text_right=text_right,
        text_left=text_left,
        image=image,
        fa_icon=fa_icon,
        separator=separator,
        bg_color=bg_color,
        text_color=text_color,
        time=time,
    )

    return "", 200


@app.route("/api/v1/export")
def serve_api_data():
    return jsonify(get_data())


@app.route("/api/v1/search/<path:search>")
def serve_api_search(search):
    return jsonify(get_data("*" + search + "*"))


@app.route("/api/v1/search_prefix/<path:search>")
def serve_api_search_prefix(search):
    return jsonify(get_data(search + "/*"))


@app.route("/api/v1/premix_list")
def serve_api_premix_list():
    keys = get_keys("premix/*")

    data = []

    for key in keys:
        name = key.split("/")[1]
        if name not in data:
            data.append(name)

    return jsonify(data)


@app.route("/api/v1/overlay_list")
def serve_api_overlay_list():
    keys = get_keys("overlay/*")

    data = []

    for key in keys:
        name = key.split("/")[1]
        if name not in data:
            data.append(name)

    return jsonify(data)


@app.route("/api/v1/restore_list")
def serve_api_restore_list():
    return jsonify(list_restore_file_names())


@app.route("/api/v1/restore_properties")
def serve_api_restore_properties():
    return jsonify(get_restore_properties_cache())


@app.route("/api/v1/reload_restore_properties", methods=["POST"])
def serve_api_reload_restore_properties():

    executor.submit(list_restore_properties)

    return "", 200


@app.route("/api/v1/get/<path:path>")
def serve_api_get(path):
    return str(REDIS_CONNECTION.get(path))


@app.route("/api/v1/set/<path:path>", methods=["POST"])
def serve_api_set(path):
    send_data(path, request.form["value"])
    return "", 200


@app.route("/api/v1/easy_set/<path:path>/<value>", methods=["POST"])
def serve_api_set_easy(path, value):
    def do_work(path, value):
        send_data(path, value)

    thread = Thread(target=do_work, kwargs={"path": path, "value": value}, daemon=True)
    thread.start()
    return "", 200


@app.route("/api/v1/set_multiple", methods=["POST"])
def serve_api_set_multiple():
    def do_work(json):
        send_data_multiple(request.get_json(force=True))

    thread = Thread(
        target=do_work, kwargs={"json": request.get_json(force=True)}, daemon=True
    )
    thread.start()
    return "", 200


@app.route("/api/v1/set_multiple_for_path_and_flatten/<path:path>", methods=["POST"])
def serve_api_set_multiple_for_path_and_flatten(path):
    def do_work(json, path):
        send_data_multiple(flatten(json, parent_key=path))

    thread = Thread(
        target=do_work,
        kwargs={"json": request.get_json(force=True), "path": path},
        daemon=True,
    )
    thread.start()
    return "", 200


@app.route("/api/v1/restore_default/<path:path>", methods=["POST"])
def serve_api_restore_default(path):
    def do_work(path):
        run_script("restore/" + path + ".yaml", None)

    thread = Thread(target=do_work, kwargs={"path": path}, daemon=True)
    thread.start()
    return "", 200


@app.route("/api/v1/restore/<path:path>/<value>", methods=["POST"])
def serve_api_restore(path, value):

    thread = Thread(
        target=run_script,
        kwargs={"restore_filename": "restore/" + path + ".yaml", "script_name": value},
        daemon=True,
    )
    thread.start()
    return "", 200


@app.route("/api/v1/list_data_sources")
def serve_api_list_data_sources():
    return jsonify(get_data_source_list_formatted())


@app.route("/api/v1/run_data_source/<path:path>", methods=["POST", "DELETE"])
def serve_api_run_data_source(path):
    if request.method == "POST":
        logger.info(
            "Requested data_source run: %s - %s", path, request.get_json(force=True)
        )
        run_data_source_in_background(path, **request.get_json(force=True))
        return "", 200
    elif request.method == "DELETE":
        logger.info("Stopping data_source: %s", path)
        stop_data_source(path)
        return "", 200


@app.route("/gamestate/csgo", methods=["POST"])
def serve_gamestate_csgo():
    data = request.json

    # if 'previously' in data:
    #     del data['previously']
    # if 'added' in data:
    #     del data['added']
    # if 'grenades' in data:
    #     del data['grenades']
    # if 'provider' in data:
    #     del data['provider']

    # send_data_multiple(flatten(data, parent_key='data/net/counter-strike/game-state-raw'))

    parsed_data = parse_csgo_data(data)

    send_data_multiple(
        flatten(parsed_data, parent_key="data/net/counter-strike/game-state")
    )

    return "", 200, {"Content-Type": "text/html"}


@sio.on("connect", namespace="/websocket")
def sio_connect():
    logger.info("socket_io connect")


@sio.on("disconnect", namespace="/websocket")
def sio_disconnect():
    logger.info("socket_io disconnect")


def redis_update_handler(message):
    logger.info("redis update: %s", message["channel"])
    if message["channel"] == "__keyspace@0__:all_data":
        key = REDIS_CONNECTION.lpop("event")
        sio.emit(
            "update",
            {
                "path": key,
                "data": REDIS_CONNECTION.hget("all_data", key).decode("utf-8"),
            },
            namespace="/websocket",
        )


sass.compile(
    dirname=(__package__ + "/static/sass", __package__ + "/static/css"),
    output_style="compressed",
)

# p = REDIS_CONNECTION.pubsub(ignore_subscribe_messages=True)
# p.psubscribe(**{'__keyspace@0__:*': redis_update_handler})
# thread = p.run_in_thread(daemon=True)

logger.info("Running cleanup process.")

# delete_keys(search="data/net/counter-strike/game-state/*")
delete_keys(search="data/net/counter-strike/game-state-raw/*")
delete_keys(search="data/com/indycar/*")
delete_keys(search="data/com/nascar/*")
# delete_keys(search="data/net/minecraft/*")

logger.info("Cleanup process complete.")

# http_server = WSGIServer(("", 8000), app, log=werkzeug_log)
# http_server.serve_forever()
