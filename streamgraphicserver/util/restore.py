import glob
import logging
import os
import pprint
import threading
import time

from ruamel.yaml import YAML

from semantic_version import Version, SimpleSpec

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

yaml = YAML()

restore_list = []


def run_script(restore_filename, script_name=None):
    logger.info("Restoring (w/ thread):", restore_filename, script_name)
    data = restore_file(restore_filename)

    if script_name:
        thread = threading.Thread(
            target=run_next_script, kwargs={"data": data, "script_name": script_name}
        )
        thread.start()
    else:
        run_init(data)


def run_script_no_thread(restore_filename, script_name=None):
    logger.info("Restoring:", restore_filename, script_name)
    data = restore_file(restore_filename)

    if script_name:
        run_next_script(data, script_name)
    else:
        run_init(data)


def restore_file(restore_filename):
    with open(restore_filename) as yaml_backup:
        data = yaml.load(yaml_backup)

        if "header" in data:
            if "version" in data["header"]:
                if not Version(data["header"]["version"]) in SimpleSpec(
                    ">=9.0.0,<10.0.0"
                ):
                    logger.error("ERROR: Restore file version unsupported.")
            if "dependencies" in data["header"]:
                for dependency in data["header"]["dependencies"]:
                    if "script" in dependency:
                        run_script_no_thread(
                            "restore/" + dependency["filename"] + ".yaml",
                            dependency["script"],
                        )
                    else:
                        run_script_no_thread(
                            "restore/" + dependency["filename"] + ".yaml", None
                        )
        return data


def run_init(data):
    if "init" in data:
        send_data_multiple(flatten(data["init"]))


def run_next_script(data, script_name):
    logger.info("----- Starting Script: %s -----", script_name)
    logger.info(pprint.pformat(data["scripts"].keys()))
    if "dependency" in data["scripts"][script_name]:
        dependency = data["scripts"][script_name]["dependency"]

        logger.info(
            "--- Running Dependency: "
            + dependency["file"]
            + " "
            + dependency["script"]
            + " ---"
        )

        run_script("restore/" + dependency["file"] + ".yaml", dependency["script"])
    for script_line in data["scripts"][script_name]["timeline"]:
        if "time_after" in script_line:
            time.sleep(script_line["time_after"])
        else:
            time.sleep(1)

        logger.info(pprint.pformat(script_line))
        if "changes" in script_line:
            send_data_multiple(flatten(script_line["changes"]))


def list_restore_files():
    return [x for x in glob.iglob("restore/**/*.yaml", recursive=True)]


def list_restore_file_names():
    return [
        "/".join(x.split(os.path.sep)[1:])[: -len(".yaml")]
        for x in list_restore_files()
    ]


def get_restore_properties_cache():
    global restore_list
    
    if (len(restore_list) > 0):
        return restore_list
    else:
        list_restore_properties()
        return restore_list


def list_restore_properties():
    import time
    global restore_list

    restore_list_tmp = []

    for restore_filename in list_restore_files():
        with open(restore_filename) as yaml_backup:
            tic = time.perf_counter()
            data = yaml.load(yaml_backup)

            short_name = "/".join(restore_filename.split(os.path.sep)[1:])[
                : -len(".yaml")
            ]
            toc = time.perf_counter()

            print("Read", short_name, f"in {toc - tic:0.4f} seconds")

            if "header" in data:
                restore_list_tmp.append(
                    {
                        "name": short_name,
                        "supported": False,
                        "has_init": False,
                        "scripts": [],
                    }
                )

                if "version" in data["header"]:
                    if Version(data["header"]["version"]) in SimpleSpec(
                        ">=9.0.0,<10.0.0"
                    ):
                        restore_list_tmp[-1]["supported"] = True
                        if "init" in data:
                            restore_list_tmp[-1]["has_init"] = True

                        if "scripts" in data:
                            for script in data["scripts"]:
                                restore_list_tmp[-1]["scripts"].append(
                                    {"name": script, "timeline": [], "total_time": 0}
                                )
                                if "timeline" in data["scripts"][script]:
                                    for timeline_item in data["scripts"][script][
                                        "timeline"
                                    ]:
                                        if "time_after" in timeline_item:
                                            time_after = timeline_item["time_after"]
                                            restore_list_tmp[-1]["scripts"][-1][
                                                "timeline"
                                            ].append(time_after)
                                            restore_list_tmp[-1]["scripts"][-1][
                                                "total_time"
                                            ] += time_after

    restore_list = restore_list_tmp


if __name__ == "__main__":
    logger.info(pprint.pformat(list_restore_file_names()))
    logger.info(pprint.pformat(list_restore_properties()))
    run_script("restore/csgo/csgo-gsi-raw-test.yaml", None)
    # run_script('restore/enzanki-ars/testing.yaml', None)
    # run_script('restore/system/now-playing-new.yaml', "middle-large")
    # run_script('restore/system/now-playing-new.yaml', "bottom-small")
    # run_script('restore/osuesports/bottom-ticker.yaml', None)
    # run_script('restore/indycar/indycar-indy-500.yaml', None)
    # run_script('restore/indycar/indycar-indy-500-driver-on-track.yaml', None)
    # run_script('restore/indycar/indycar-indy-500-driver-telemetry.yaml', None)
    # run_script('restore/indycar/indycar-indy-500-lines.yaml', None)
    # run_script('restore/indycar/indycar-flag-video.yaml', None)
    # run_script('restore/csgo/hltv-live.yaml', None)
