import logging

from streamgraphicserver.util.data import get_keys, get_item, delete_key, send_data

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    for mode in ["16x9"]:
        for component in get_keys("*/style/pos-" + mode + "-x-b"):
            component_name = component.split("/")[1]
            component_style_prefix = "/".join(component.split("/")[0:3])

            x_b = int(get_item(component_style_prefix + "/pos-" + mode + "-x-b"))
            x_e = int(get_item(component_style_prefix + "/pos-" + mode + "-x-e"))
            y_b = int(get_item(component_style_prefix + "/pos-" + mode + "-y-b"))
            y_e = int(get_item(component_style_prefix + "/pos-" + mode + "-y-e"))

            x = x_b
            w = x_e - x
            y = y_b
            h = y_e - y

            # logger.info("%s - %s %s %s %s", component, x_b, x_e, y_b, y_e)

            if (x_b >= x_e) or (y_b >= y_e):
                logger.error(
                    "Validation Error: %s - %s %s %s %s", component, x_b, x_e, y_b, y_e
                )
            else:
                delete_key(component_style_prefix + "/pos-" + mode + "-x-b")
                delete_key(component_style_prefix + "/pos-" + mode + "-x-e")
                delete_key(component_style_prefix + "/pos-" + mode + "-y-b")
                delete_key(component_style_prefix + "/pos-" + mode + "-y-e")

                send_data(component_style_prefix + "/pos-" + mode + "-x", x)
                send_data(component_style_prefix + "/pos-" + mode + "-w", w)
                send_data(component_style_prefix + "/pos-" + mode + "-y", y)
                send_data(component_style_prefix + "/pos-" + mode + "-h", h)

                logger.info(
                    "%s - %s %s %s %s --> %s %s %s %s",
                    component,
                    x_b,
                    x_e,
                    y_b,
                    y_e,
                    x,
                    w,
                    y,
                    h,
                )
