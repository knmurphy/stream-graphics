import os

import redis
from flask_socketio import SocketIO

cache = {}


def get_item(key):
    return REDIS_CONNECTION.hget("all_data", key)


def get_data(search=None):
    data = {}

    for key, value in REDIS_CONNECTION.hscan_iter(
        "all_data", match=search, count=1000000000
    ):
        data[key] = value

    return data


def get_keys(search=None):
    data = []

    for key, value in REDIS_CONNECTION.hscan_iter(
        "all_data", match=search, count=1000000000
    ):
        data.append(key)

    return data


def delete_key(key):
    REDIS_CONNECTION.hdel("all_data", key)


def delete_keys(search=None):
    keys_to_del = get_keys(search=search)

    if len(keys_to_del) > 0:
        REDIS_CONNECTION.hdel("all_data", *keys_to_del)


def update_data(path, data):
    global cache
    # print(path, str(data))

    if path not in cache.keys() or cache[path] != data:
        if data is None:
            data = ""

        cache[path] = data

        # print(path, data)

        REDIS_CONNECTION.hset("all_data", path, str(data))
        socket_io.emit(
            "update", {"path": path, "data": str(data)}, namespace="/websocket"
        )


def send_event_log(
    log_name,
    text_right="",
    text_left="",
    fa_icon="",
    image="",
    separator=": ",
    bg_color="black",
    text_color="white",
    time=5000,
):
    socket_io.emit(
        "event_log",
        {
            "log_name": log_name,
            "text_right": text_right,
            "text_left": text_left,
            "image": image,
            "fa_icon": fa_icon,
            "separator": separator,
            "bg_color": bg_color,
            "text_color": text_color,
            "time": time,
        },
        namespace="/websocket",
    )


def send_data(path, data):
    update_data(path, data)


def send_data_multiple(all_data):
    for key, value in all_data.items():
        send_data(key, value)


REDIS_IP = os.getenv("REDIS_IP", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", "6379")
REDIS_DB = os.getenv("REDIS_DB", "0")
REDIS_HOST = "redis://" + REDIS_IP + ":" + str(REDIS_PORT)
REDIS_CONNECTION = redis.StrictRedis(
    host=REDIS_IP, port=REDIS_PORT, db=REDIS_DB, decode_responses=True
)
socket_io = SocketIO(async_mode="gevent", message_queue=REDIS_HOST)
