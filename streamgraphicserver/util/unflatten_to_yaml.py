from flatten_json import flatten, unflatten
from ruamel.yaml import YAML

import copy
import os
import json
from pathlib import Path
from glob import iglob

for filename in iglob("restore_outdated_v1/enzanki-ars/*.json", recursive=True):
    with open(filename) as json_file:
        data = json.load(json_file)
        unflattened_init = unflatten(
            flatten(data["init"], separator="/"), separator="/"
        )

        new_json = copy.deepcopy(data)

        new_json["init"] = unflattened_init

        new_path_name = filename.replace(".json", ".yaml").replace(
            "restore_outdated_v1", "restore"
        )
        new_path_file = Path(new_path_name)
        new_path = Path("/".join(new_path_name.split("/")[:-1]))

        new_path.mkdir(parents=True, exist_ok=True)

        yaml = YAML()
        yaml.default_flow_style = False
        yaml.dump(new_json, Path(new_path_file))
