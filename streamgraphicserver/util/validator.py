from flatten_json import flatten, unflatten
from ruamel.yaml import YAML
from jsonschema import validate, ValidationError

import copy
import os
import json
import sys
from pathlib import Path
from glob import iglob

exit_code = 0

with open("schema.json") as schema_json:
    schema = json.load(schema_json)

    for filename in iglob("restore/**/*.yaml", recursive=True):
        print("Validating", filename)
        with open(filename) as yaml_file:
            yaml = YAML()
            data = yaml.load(yaml_file)

            unflattened = unflatten(
                flatten(data, separator="/"), separator="/"
            )
            try:
                validate(instance=unflattened, schema=schema)
            except ValidationError as error:
                print("ERROR:", error, file=sys.stderr)
                exit_code = 1

exit(exit_code)
