import collections


def flatten(d, parent_key="", sep="/"):
    # Modified from https://stackoverflow.com/a/6027615

    items = []

    if isinstance(d, dict):
        for k, v in d.items():
            new_key = parent_key + sep + str(k) if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(flatten(v, new_key, sep=sep).items())
            elif isinstance(v, list):
                for i, item in enumerate(v):
                    new_new_key = new_key + sep + str(i)
                    items.extend(flatten(item, new_new_key, sep=sep).items())
            else:
                items.append((new_key, v))
    else:
        items.append((parent_key, d))
    return dict(items)


def get_path_from_dict(data, path):
    curr = data
    while len(path):
        key = path.pop(0)
        curr = curr.get(key)
        if type(curr) is not dict and len(path):
            return ""
    if curr is not None:
        return curr
    else:
        return ""
