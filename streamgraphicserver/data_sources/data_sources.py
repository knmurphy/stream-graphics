import copy
import datetime
import logging
from multiprocessing import Process

import streamgraphicserver.data_sources.com.twitter.tweet
import streamgraphicserver.data_sources.com.ncaa.scores

# import streamgraphicserver.data_sources.com.rocketleague.parse_replay
import streamgraphicserver.data_sources.edu.osu.emergency
import streamgraphicserver.data_sources.io.gitlab.osubgc.results
import streamgraphicserver.data_sources.system.time.countdown
import streamgraphicserver.data_sources.system.time.current_time
import streamgraphicserver.data_sources.system.vlc.now_playing

data_source_list = {
    "com/twitter/tweet": {
        "run": streamgraphicserver.data_sources.com.twitter.tweet.run,
        "parameters": {
            "tweet": {
                "default": "",
                "documentation": "Tweet number (last part of URL).",
            }
        },
        "process": None,
        "run_parameters": {},
    },
    "com/ncaa/scores": {
        "run": streamgraphicserver.data_sources.com.ncaa.scores.run,
        "parameters": {
            "game": {"default": "", "documentation": "Game number (last part of URL).",}
        },
        "process": None,
        "run_parameters": {},
    },
    # "com/rocketleague/parse_replay": {
    #     "run": streamgraphicserver.data_sources.com.rocketleague.parse_replay.run,
    #     "parameters": {},
    #     "process": None,
    #     "run_parameters": {},
    # },
    "edu/osu/emergency": {
        "run": streamgraphicserver.data_sources.edu.osu.emergency.run,
        "parameters": {
            "test_mode": {
                "default": "",
                "documentation": "Leave blank for normal mode. Enter anything to start TEST mode.",
            }
        },
        "process": None,
        "run_parameters": {},
    },
    "io/gitlab/osubgc/results": {
        "run": streamgraphicserver.data_sources.io.gitlab.osubgc.results.run,
        "parameters": {},
        "process": None,
        "run_parameters": {},
    },
    "system/vlc/now_playing": {
        "run": streamgraphicserver.data_sources.system.vlc.now_playing.run,
        "parameters": {},
        "process": None,
        "run_parameters": {},
    },
    "system/time/countdown": {
        "run": streamgraphicserver.data_sources.system.time.countdown.run,
        "parameters": {
            "countdown": {
                "default": str(
                    datetime.datetime.now().replace(second=0, microsecond=0).isoformat()
                ),
                "type": "datetime-local",
                "documentation": "Countdown to.",
            }
        },
        "process": None,
        "run_parameters": {},
    },
    "system/time/current_time": {
        "run": streamgraphicserver.data_sources.system.time.current_time.run,
        "parameters": {},
        "process": None,
        "run_parameters": {},
    },
}


def get_data_source_list():
    return data_source_list


def get_data_source_list_formatted():
    data_source_list_edited = copy.deepcopy(get_data_source_list())

    for data_source in data_source_list_edited:
        del data_source_list_edited[data_source]["run"]
        del data_source_list_edited[data_source]["process"]

    return data_source_list_edited


def return_default_parameters(data_source):
    defaults = {}

    for parameter in data_source_list[data_source]["parameters"]:
        defaults[parameter] = data_source_list[data_source]["parameters"][parameter][
            "default"
        ]

    return defaults


def override_parameters(data_source, **kwargs):
    options = return_default_parameters(data_source)

    for key, value in kwargs.items():
        options[key] = value

    return options


def run_data_source(data_source, **kwargs):
    data_source_list[data_source]["run"](**override_parameters(data_source, **kwargs))


def stop_data_source(data_source):
    if (
        data_source_list[data_source]["process"] is not None
        and data_source_list[data_source]["process"].is_alive()
    ):
        data_source_list[data_source]["process"].terminate()


def run_data_source_in_background(data_source, **kwargs):
    data_logger = logging.getLogger(__name__)

    if (
        data_source_list[data_source]["process"] is not None
        and data_source_list[data_source]["process"].is_alive()
    ):
        if data_source_list[data_source]["run_parameters"] == {**kwargs}:
            data_logger.info("Data source %s is already running.", data_source)
        else:
            data_logger.info("Restarting data source %s with new config.", data_source)
            data_source_list[data_source]["process"].terminate()
            start_data_source_in_background(data_source, **kwargs)
    else:
        start_data_source_in_background(data_source, **kwargs)


def start_data_source_in_background(data_source, **kwargs):
    data_logger = logging.getLogger(__name__)
    data_logger.info(
        "Starting data source %s in background with config: %s",
        data_source,
        ", ".join("{0}={1!r}".format(k, v) for k, v in kwargs.items()),
    )
    p = Process(target=run_data_source, args=(data_source,), kwargs={**kwargs})
    p.daemon = True
    p.start()
    data_logger.info("Now running data source %s in background.", data_source)
    data_source_list[data_source]["process"] = p
    data_source_list[data_source]["run_parameters"] = {**kwargs}


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.error("This should not be run on it's own...")
