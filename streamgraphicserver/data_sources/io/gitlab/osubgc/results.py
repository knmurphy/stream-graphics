import logging
import multiprocessing
import sched
import time

import requests

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.restore import run_script

# HOSTNAME = "https://osubgc.gitlab.io"
HOSTNAME = "http://localhost:4000"

results = []
games = {}
leagues = {}

curr_pos = 0


def update_data(scheduler):
    global results
    global games
    global leagues
    global opponents

    logger = logging.getLogger(__name__)

    try:
        logger.info("Requesting results.")
        r = requests.get(HOSTNAME + "/api/results.json", timeout=(3.05, 27))
        results = r.json()

        logger.info("Requesting games.")
        g = requests.get(HOSTNAME + "/api/games.json", timeout=(3.05, 27))
        games = g.json()

        for game in games:
            games[game]["logo"] = HOSTNAME + games[game]["logo"]
            games[game]["custom-style"] = (
                "background-color: " + games[game]["color"] + ";"
            )

        logger.info("Requesting leagues.")
        l = requests.get(HOSTNAME + "/api/leagues.json", timeout=(3.05, 27))
        leagues = l.json()

        for league in leagues:
            leagues[league]["logo"] = HOSTNAME + leagues[league]["logo"]
            leagues[league]["custom-style"] = (
                "background-color: " + leagues[league]["color"] + ";"
            )

        # send_data_multiple(flatten(info, parent_key='data/io/gitlab/osubgc/results'))
    except requests.exceptions.ConnectionError:
        logger.error("Failed to connect for previous request.")
        pass

    scheduler.enter(60, 1, update_data, argument=(scheduler,))


def update_cycle(scheduler):
    global results
    global games
    global leagues

    global curr_pos

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    if len(results) > 0:
        # Ensure that curr_pos is within the displayable list.
        curr_pos = curr_pos % len(results)
    else:
        pass

    # curr_pos = 0

    logger.info("Displaying %i/%i", curr_pos + 1, len(results))

    do_transition_show()

    send_data_multiple(
        flatten(results[curr_pos], parent_key="data/io/gitlab/osubgc/results/cycle")
    )

    send_data_multiple(
        flatten(
            games[results[curr_pos]["game"]],
            parent_key="data/io/gitlab/osubgc/results/cycle/game",
        )
    )
    send_data_multiple(
        flatten(
            leagues[results[curr_pos]["league"]],
            parent_key="data/io/gitlab/osubgc/results/cycle/league",
        )
    )

    do_transition_hide()
    logger.info("Done displaying %i/%i", curr_pos + 1, len(results))

    # Move to the next result
    curr_pos = (curr_pos + 1) % len(results)

    scheduler.enter(12.5, 1, update_cycle, argument=(scheduler,))


def do_transition_show():
    # run_script("restore/edu/osu/osubgc/bottom-ticker.yaml", "ticker_transition_show")
    time.sleep(0.5)


def do_transition_hide():
    # run_script("restore/edu/osu/osubgc/bottom-ticker.yaml", "ticker_transition_hide")
    time.sleep(0.5)


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting io/gitlab/osubgc/results data_source.")

    scheduler = sched.scheduler(time.time, time.sleep)
    scheduler.enter(0, 1, update_data, argument=(scheduler,))
    scheduler.enter(0, 1, update_cycle, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
