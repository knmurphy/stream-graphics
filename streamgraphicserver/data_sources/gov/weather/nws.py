import requests

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten

test_mode = True


def update_weather(lat, lon):
    meta = requests.get("https://api.weather.gov/points/" + str(lat) + "," + str(lon))
    forecast = requests.get(meta.json()["properties"]["forecast"])

    send_data_multiple(
        flatten(forecast.json()["properties"], parent_key="data/gov/weather/forecast")
    )


if __name__ == "__main__":
    update_weather(39.999436, -83.012724)
