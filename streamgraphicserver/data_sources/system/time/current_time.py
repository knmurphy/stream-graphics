import datetime
import logging
import multiprocessing
import sched
import time

import pytz

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten


def get_time(scheduler):
    data = {}

    now = datetime.datetime.now(pytz.utc)

    data["utc"] = now.strftime("UTC: %Y-%m-%d %H:%M:%S")
    data["pt"] = now.astimezone(pytz.timezone("US/Pacific")).strftime(
        "PT: %Y-%m-%d %H:%M:%S"
    )
    data["et"] = now.astimezone(pytz.timezone("US/Eastern")).strftime(
        "ET: %Y-%m-%d %H:%M:%S"
    )
    data["both-utc-et"] = data["utc"] + " | " + data["et"]
    data["et-short"] = now.astimezone(pytz.timezone("US/Eastern")).strftime("%-I:%M %p")

    send_data_multiple(flatten(data, parent_key="data/system/time"))

    scheduler.enter(1, 1, get_time, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting system/time/current_time data_source.")

    scheduler = sched.scheduler(time.time, time.sleep)
    scheduler.enter(0, 1, get_time, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
