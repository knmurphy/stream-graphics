import logging
import multiprocessing
import os
import sched
import time
from shutil import copyfile
from urllib.parse import unquote

import requests
from slugify import slugify

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

connected_to_vlc = None

data_sent = False

old_play_info = {"time": 0, "length": 0, "position": 0, "state": 0}

old_now_playing = {
    "album": "",
    "artist": "",
    "title": "",
    "album-art": "",
    "album-art-html": "",
}


def get_vlc_info(scheduler):
    global old_play_info
    global old_now_playing
    global data_sent
    global connected_to_vlc

    logger = logging.getLogger(__name__)

    play_info = {"time": 0, "length": 0, "position": 0, "state": "stopped"}
    now_playing = {
        "album": "",
        "artist": "",
        "title": "",
        "album-art": "",
        "album-art-html": "",
    }

    try:
        r = requests.get(
            "http://172.27.16.1:8080/requests/status.json",
            auth=("", "stream"),
            timeout=(3.05, 27),
        )

        if not connected_to_vlc:
            logger.info("Reconnected to VLC.")
            connected_to_vlc = True

        r.encoding = "utf-8"

        r_json = r.json()

        time_m, time_s = divmod(r_json["time"], 60)
        len_m, len_s = divmod(r_json["length"], 60)

        pos_html = str(r_json["position"] * 100)

        if r_json["state"] == "playing":
            state_icon = "fas fa-play"
        elif r_json["state"] == "paused":
            state_icon = "fas fa-pause"
        elif r_json["state"] == "stopped":
            state_icon = "fas fa-stop"
        else:
            state_icon = "fas fa-question-circle"

        play_info = {
            "time": r_json["time"],
            "time_readable": "{:d}:{:02d}".format(time_m, time_s),
            "length": r_json["length"],
            "length_readable": "{:d}:{:02d}".format(len_m, len_s),
            "position": r_json["position"],
            "position_html": pos_html,
            "state": r_json["state"],
            "state_icon": state_icon,
        }

        if "information" in r_json.keys():
            meta = r_json["information"]["category"]["meta"]

            if "album" in meta.keys():
                now_playing["album"] = meta["album"]
            if "artist" in meta.keys():
                now_playing["artist"] = meta["artist"]
            if "title" in meta.keys():
                now_playing["title"] = meta["title"]

            if "artwork_url" in meta:
                orig_art_url = unquote(meta["artwork_url"])

                copyfile(
                    unquote(orig_art_url).replace("file:///C:/", "/mnt/c/"),
                    os.path.join("streamgraphicserver", "static", "local", "art"),
                )
                album_art_url = "/static/local/art?" + slugify(
                    meta["artist"] + "-" + meta["title"]
                )

                now_playing["album-art"] = album_art_url
                now_playing["album-art-html"] = '<img src="' + album_art_url + '">'
    except requests.exceptions.ConnectionError:
        if connected_to_vlc is None:
            logger.error("Connection to VLC failed. Retrying...")
        elif connected_to_vlc:
            logger.error("Disconnected from VLC. Retrying...")
        connected_to_vlc = False

    if play_info != old_play_info or not data_sent:
        send_data_multiple(flatten(play_info, parent_key="data/system/vlc"))
        old_play_info = play_info

    if now_playing != old_now_playing or not data_sent:
        send_data_multiple(
            flatten(now_playing, parent_key="data/system/vlc/now-playing")
        )
        old_now_playing = now_playing

    if not data_sent:
        data_sent = True

    scheduler.enter(1, 1, get_vlc_info, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting system/vlc/now_playing data_source.")

    scheduler = sched.scheduler(time.time, time.sleep)
    scheduler.enter(0, 1, get_vlc_info, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
