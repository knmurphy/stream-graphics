import json
import logging
import struct
import zlib

from google.protobuf import json_format

from streamgraphicserver.data_sources.com.indycar import ErpMessage_pb2
from streamgraphicserver.util.data import send_data, send_data_multiple
from streamgraphicserver.util.other import flatten

logger = logging.getLogger(__name__)

indycar_live_tc_data = {}


def parse_message(message):
    message_len = struct.unpack(">i", message[0:4])
    message_end = 4 + message_len[0]

    payload_len = message_end + 4

    end_len = struct.unpack(">i", message[message_end:payload_len])

    payload_buf = message[payload_len : payload_len + end_len[0]]

    decompress_message = zlib.decompress(payload_buf, 16 + zlib.MAX_WBITS)

    erp_message = ErpMessage_pb2.ErpMessage()

    erp_message.ParseFromString(decompress_message)

    json_version = json.loads(json_format.MessageToJson(erp_message))

    send_data_multiple(
        flatten(
            json_version["heartbeats"][0],
            parent_key="data/com/indycar/ts-raw/heartbeat",
        )
    )
    send_data_multiple(
        flatten(
            json_version["isDifferential"],
            parent_key="data/com/indycar/ts-raw/isDifferential",
        )
    )
    send_data_multiple(
        flatten(json_version["version"], parent_key="data/com/indycar/ts-raw/version")
    )

    send_data(
        "data/com/indycar/ts/lap_info/completed_laps",
        json_version["heartbeats"][0]["completedLaps"],
    )
    send_data(
        "data/com/indycar/ts/lap_info/total_laps",
        json_version["heartbeats"][0]["totalLaps"],
    )

    send_data(
        "data/com/indycar/ts/lap_info/lap_info",
        str(json_version["heartbeats"][0]["completedLaps"])
        + "/"
        + str(json_version["heartbeats"][0]["totalLaps"]),
    )

    if "messages" in json_version:
        send_data_multiple(
            flatten(
                json_version["messages"][0],
                parent_key="data/com/indycar/ts-raw/message",
            )
        )

    if "telemetryMessages" in json_version:
        for telemetry_message in json_version["telemetryMessages"]:
            send_data_multiple(
                flatten(
                    telemetry_message,
                    parent_key="data/com/indycar/ts-raw/telemetryMessages/"
                    + str(telemetry_message["carNumber"]),
                )
            )

    if "completedLapResult" in json_version:
        for completed_lap_result in json_version["completedLapResult"]:
            send_data_multiple(
                flatten(
                    completed_lap_result,
                    parent_key="data/com/indycar/ts-raw/completedLapResult/"
                    + str(completed_lap_result["carNumber"]),
                )
            )

    if "overallResults" in json_version:
        for overall_result in json_version["overallResults"]:
            send_data_multiple(
                flatten(
                    overall_result,
                    parent_key="data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"]),
                )
            )
            send_data_multiple(
                flatten(
                    overall_result,
                    parent_key="data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"]),
                )
            )

            if (
                "lastWarmUpQualTime" in overall_result
                and int(overall_result["lastWarmUpQualTime"]) > 0
            ):
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lastWarmUpQualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lastWarmUpQualTime"]) / 10000))
                        * 60
                        * 60
                    ),
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lastWarmUpQualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lastWarmUpQualTime"]) / 10000))
                        * 60
                        * 60
                    ),
                )
            else:
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lastWarmUpQualMPH",
                    "---.---",
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lastWarmUpQualMPH",
                    "---.---",
                )

            if (
                "lap1QualTime" in overall_result
                and int(overall_result["lap1QualTime"]) > 0
            ):
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap1QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap1QualTime"]) / 10000)) * 60 * 60
                    ),
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap1QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap1QualTime"]) / 10000)) * 60 * 60
                    ),
                )
            else:
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap1QualMPH",
                    "---.---",
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap1QualMPH",
                    "---.---",
                )

            if (
                "lap2QualTime" in overall_result
                and int(overall_result["lap2QualTime"]) > 0
            ):
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap2QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap2QualTime"]) / 10000)) * 60 * 60
                    ),
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap2QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap2QualTime"]) / 10000)) * 60 * 60
                    ),
                )
            else:
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap2QualMPH",
                    "---.---",
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap2QualMPH",
                    "---.---",
                )

            if (
                "lap3QualTime" in overall_result
                and int(overall_result["lap3QualTime"]) > 0
            ):
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap3QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap3QualTime"]) / 10000)) * 60 * 60
                    ),
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap3QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap3QualTime"]) / 10000)) * 60 * 60
                    ),
                )
            else:
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap3QualMPH",
                    "---.---",
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap3QualMPH",
                    "---.---",
                )

            if (
                "lap4QualTime" in overall_result
                and int(overall_result["lap4QualTime"]) > 0
            ):
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap4QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap4QualTime"]) / 10000)) * 60 * 60
                    ),
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap4QualMPH",
                    "{:3.3f}".format(
                        (2.5 / (int(overall_result["lap4QualTime"]) / 10000)) * 60 * 60
                    ),
                )
            else:
                send_data(
                    "data/com/indycar/ts-raw/overallResults/"
                    + str(overall_result["carNumber"])
                    + "/lap4QualMPH",
                    "---.---",
                )
                send_data(
                    "data/com/indycar/ts-raw/overallRank/"
                    + str(overall_result["overallRank"])
                    + "/lap4QualMPH",
                    "---.---",
                )

    if "lineCrossings" in json_version:
        for line_crossing in json_version["lineCrossings"]:
            send_data_multiple(
                flatten(
                    line_crossing,
                    parent_key="data/com/indycar/ts-raw/lineCrossings/"
                    + str(line_crossing["carNumber"]),
                )
            )

    if "entries" in json_version:
        for entry in json_version["entries"]:
            send_data_multiple(
                flatten(
                    entry,
                    parent_key="data/com/indycar/ts-raw/entries/"
                    + str(entry["carNumber"]),
                )
            )


def run():
    from lomond import WebSocket, persist, events

    global indycar_live_tc_data
    global updated_data

    websocket = WebSocket("ws://turbine-prod-vir.indy.myomnigon.com:443")

    logger.info("Connected to Indycar websocket")

    for event in persist.persist(websocket):
        if isinstance(event, events.Ready):
            websocket.send_text(
                "TURBINE\nlogin:guest\npass:guest\ndest:prod_snapshot\n\n\0\n"
            )
        if isinstance(event, events.Binary):
            parse_message(event.data)

            logger.info("Parsed Indycar message.")


if __name__ == "__main__":
    logger.error("This should not be run independently.")
    # run()
