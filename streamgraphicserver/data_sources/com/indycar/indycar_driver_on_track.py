from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten

if __name__ == "__main__":
    CAR_NUM = "15"

    indycar_on_track = {
        "component/indycar-indy-500-on-track-number/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/overallRank",
        "component/indycar-indy-500-on-track-name/data": "com/indycar/ts-raw/entries/"
        + CAR_NUM
        + "/driverName",
        "component/indycar-indy-500-on-track-engine/data": "com/indycar/ts-raw/entries/"
        + CAR_NUM
        + "/engine",
        "component/indycar-indy-500-on-track-team/data": "com/indycar/ts-raw/entries/"
        + CAR_NUM
        + "/team",
        "component/indycar-indy-500-on-track-lap-0-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/lastWarmUpQualMPH",
        "component/indycar-indy-500-on-track-lap-1-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/lap1QualMPH",
        "component/indycar-indy-500-on-track-lap-2-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/lap2QualMPH",
        "component/indycar-indy-500-on-track-lap-3-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/lap3QualMPH",
        "component/indycar-indy-500-on-track-lap-4-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/lap4QualMPH",
        "component/indycar-indy-500-on-track-average-speed/data": "com/indycar/ts-raw/overallResults/"
        + CAR_NUM
        + "/averageSpeed",
        "component/indycar-telemetry-speed/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/vehicleSpeed",
        "component/indycar-telemetry-break/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/breakPercentage",
        "component/indycar-telemetry-throttle/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/throttle",
        "component/indycar-telemetry-gear/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/gear",
        "component/indycar-telemetry-steering/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/steering",
        "component/indycar-telemetry-rpm/data": "com/indycar/ts-raw/telemetryMessages/"
        + CAR_NUM
        + "/engineSpeed",
    }

    send_data_multiple(flatten(indycar_on_track, parent_key=""))
