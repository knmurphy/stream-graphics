import sched
import time

import requests
from bs4 import BeautifulSoup
from slugify import slugify

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten

info = {}
data_to_send = {}
cycle_data = []
curr_pos = 0


def update_data():
    global data_to_send
    global cycle_data
    global info
    try:
        r = requests.get(
            "https://awscdn.wbns.com/feeds/schoolclosings/WBNS-schoolclosingsC.xml"
        )
        parser = BeautifulSoup(r.text, "lxml")

        data_to_send = {}
        cycle_data = []

        for closing in parser.find_all("closing"):
            curr_info = {
                "name": closing.name1.text,
                "status": closing.status.text,
                "entitytype": closing.entitytype.text,
                "city": closing.city.text,
                "county": closing.county.text,
                "state": closing.state.text,
                "updatetime": closing.updatetime.text,
            }

            data_to_send[slugify(closing.name1.text)] = curr_info

            cycle_data.append(curr_info)

        info = {"length": len(cycle_data)}
        # send_data_multiple(flatten(data_to_send, parent_key='data/com/wbns/school-closings/list'))
        # send_data_multiple(flatten(info, parent_key='data/com/wbns/school-closings/info'))
    except requests.exceptions.ConnectionError:
        pass

    s.enter(60, 1, update_data)


def update_cycle():
    global info
    global cycle_data
    global curr_pos

    if len(cycle_data) > 0:
        cycle = {
            "label": "Closings (Source: 10TV)",
            "name": cycle_data[curr_pos]["name"],
            "status": cycle_data[curr_pos]["status"],
            "entitytype": cycle_data[curr_pos]["entitytype"],
            "city": cycle_data[curr_pos]["city"],
            "county": cycle_data[curr_pos]["county"],
            "state": cycle_data[curr_pos]["state"],
            "updatetime": '<i class="fas fa-edit"></i> Last Update: '
            + cycle_data[curr_pos]["updatetime"],
            "current-out-of": '<i class="fas fa-hashtag"></i> '
            + str(curr_pos + 1)
            + "/"
            + str(info["length"]),
        }
        curr_pos = (curr_pos + 1) % info["length"]
    else:
        cycle = {
            "label": "Closings (Source: 10TV)",
            "name": "",
            "status": "",
            "entitytype": "",
            "city": "",
            "county": "",
            "state": "",
            "updatetime": '<i class="fas fa-edit"></i> Last Update: None',
            "current-out-of": '<i class="fas fa-hashtag"></i> 0/0',
        }

    send_data_multiple(flatten(cycle, parent_key="data/com/wbns/school-closings/cycle"))

    s.enter(7.5, 1, update_cycle)


if __name__ == "__main__":
    s = sched.scheduler(time.time, time.sleep)

    s.enter(0, 1, update_data)
    s.enter(0, 1, update_cycle)
    s.run()
