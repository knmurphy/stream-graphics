from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten

import requests

if __name__ == "__main__":
    r = requests.get(
        "https://api.iextrading.com/1.0/stock/market/batch?symbols="
        "DIA,SPY,IWM,amzn,nvda,aapl,fb,dis,nflx,msft,intc,amd,twtr,pypl,gpro,goog,qcom,vz,avgo,sq"
        "&types=quote"
    )
    send_data_multiple(flatten(r.json(), parent_key="data/com/iextrading/stock"))
