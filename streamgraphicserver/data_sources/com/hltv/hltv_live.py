import socketio

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten


@sio.on("scoreboard")
def on_message(data):
    send_data_multiple(flatten(data, parent_key="data/com/hltv/live"))


if __name__ == "__main__":
    sio = socketio.Client()

    sio.connect("https://cf1-scorebot.hltv.org")

    sio.emit("readyForMatch", '{"token": \'\', "listId": 2334203}')
    sio.wait()
