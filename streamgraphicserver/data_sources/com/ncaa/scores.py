import logging
import multiprocessing
import sched
import time

import requests

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

ordinal = {"1": "1st", "2": "2nd", "3": "3rd", "4": "4th"}


def update_data(scheduler=None, game="3785415"):
    logger = logging.getLogger(__name__)

    session = requests.Session()
    session.trust_env = False

    logger.info("Requesting score update.")
    score_json = session.get(
        "https://data.ncaa.com/casablanca/game/" + game + "/gameInfo.json",
        timeout=(3.05, 27),
    ).json()

    score_json["away"]["color_style"] = (
        "background-color: " + score_json["away"]["color"] + ";"
    )
    score_json["away"]["names"]["logo"] = (
        "https://i.turner.ncaa.com/sites/default/files/images/logos/schools/bgd/"
        + score_json["away"]["names"]["seo"]
        + ".svg"
    )
    score_json["home"]["color_style"] = (
        "background-color: " + score_json["home"]["color"] + ";"
    )
    score_json["home"]["names"]["logo"] = (
        "https://i.turner.ncaa.com/sites/default/files/images/logos/schools/bgd/"
        + score_json["home"]["names"]["seo"]
        + ".svg"
    )

    if score_json["status"]["down"] in ordinal:
        score_json["status"]["down-and-dist"] = (
            ordinal[score_json["status"]["down"]]
            + " & "
            + score_json["status"]["distance"]
        )
    else:
        score_json["status"]["down-and-dist"] = ""

    if score_json["status"]["possession"] == "away":
        score_json["away"]["possession"] = "fas fa-chevron-left"
        score_json["home"]["possession"] = ""
    elif score_json["status"]["possession"] == "home":
        score_json["away"]["possession"] = ""
        score_json["home"]["possession"] = "fas fa-chevron-left"
    else:
        score_json["away"]["possession"] = ""
        score_json["home"]["possession"] = ""

    send_data_multiple(
        flatten(score_json, parent_key="data/com/ncaa/selected_game/scoreboard")
    )

    scheduler.enter(15, 1, update_data, argument=(scheduler, game))


def run(game="3785415"):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting com/ncaa/scores data_source: game=%s", game)
    scheduler = sched.scheduler(time.time, time.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler, game))
    scheduler.run()


if __name__ == "__main__":
    run(True)
