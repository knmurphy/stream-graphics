import sched
import time

import requests

from streamgraphicserver.util.data import send_data_multiple
from streamgraphicserver.util.other import flatten


def update_data():
    try:
        r = requests.get(
            "https://www.nascar.com/live/feeds/series_2/4814/live_feed.json"
        )

        data_to_send = {
            "lap_number": r.json()["lap_number"],
            "laps_in_race": r.json()["laps_in_race"],
            "laps_to_go": r.json()["laps_to_go"],
            "laps": str(r.json()["lap_number"]) + "/" + str(r.json()["laps_in_race"]),
            "flag_state": r.json()["flag_state"],
            "track_name": r.json()["track_name"],
            "run_name": r.json()["run_name"],
            "full_race_name": r.json()["track_name"] + " - " + r.json()["run_name"],
            "drivers": {},
        }

        if r.json()["flag_state"] == 1:
            data_to_send["flag_name"] = "GREEN"
        elif r.json()["flag_state"] == 2:
            data_to_send["flag_name"] = "YELLOW"
        elif r.json()["flag_state"] == 3:
            data_to_send["flag_name"] = "RED"
        elif r.json()["flag_state"] == 4:
            data_to_send["flag_name"] = "CHECKERED"
        elif r.json()["flag_state"] == 5:
            data_to_send["flag_name"] = "WHITE"
        elif r.json()["flag_state"] == 8:
            data_to_send["flag_name"] = "WARM"
        elif r.json()["flag_state"] == 9:
            data_to_send["flag_name"] = "NOT ACTIVE"
        else:
            data_to_send["flag_name"] = r.json()["flag_state"]

        for vehicle in r.json()["vehicles"]:
            data_to_send["drivers"][str(vehicle["running_position"])] = vehicle

        send_data_multiple(flatten(data_to_send, parent_key="data/com/nascar/tc"))
    except requests.exceptions.ConnectionError:
        pass

    s.enter(5, 1, update_data)


if __name__ == "__main__":
    s = sched.scheduler(time.time, time.sleep)

    s.enter(0, 1, update_data)
    s.run()
