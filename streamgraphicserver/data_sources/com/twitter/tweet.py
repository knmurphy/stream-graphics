import logging
import multiprocessing

import twitter
from dateutil.parser import parse

from streamgraphicserver.util.config import get_config
from streamgraphicserver.util.data import send_data, send_data_multiple
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.restore import run_script_no_thread

logger = logging.getLogger(__name__)


def get_tweet(tweet_id):
    api = twitter.Api(**get_config()["twitter"], tweet_mode="extended")

    tweet = api.GetStatus(tweet_id)

    logger.info(tweet)
    print(tweet)

    tweet_text = tweet.full_text

    for url in tweet.urls:
        tweet_text = tweet_text.replace(
            url.url, "<a href=''>" + url.expanded_url + "</a>"
        )

    tweet_text = tweet_text.replace("\n", "<br/>")

    data = {
        "avatar": tweet.user.profile_image_url_https.replace("_normal", "_bigger"),
        "name": tweet.user.name,
        "username": "@" + tweet.user.screen_name,
        "text": tweet_text,
        "timestamp": parse(tweet.created_at).strftime("%A, %d %B %Y, %I:%M%p"),
        "images": {"1": "", "2": "", "3": "", "4": ""},
        "video": "",
    }

    has_video = False

    if tweet.media is not None:
        for i, image in enumerate(tweet.media):
            image_count = i + 1
            data["images"][str(image_count)] = tweet.media[i].media_url_https

            data["text"] = data["text"].replace(tweet.media[i].url, "")

            if tweet.media[i].type == "video" or tweet.media[i].type == "animated_gif":
                has_video = True

                largest_video = max(
                    tweet.media[i].video_info["variants"],
                    key=lambda x: x.get("bitrate", 0),
                )

                data["video"] = (
                    "<video loop autoplay muted><source src='"
                    + largest_video["url"]
                    + "' type='"
                    + largest_video["content_type"]
                    + "'></video>"
                )
    else:
        image_count = 0

    send_data_multiple(flatten(data, parent_key="data/com/twitter/tweet"))

    if has_video:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "video")
    elif image_count == 0:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-0")
    elif image_count == 1:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-1")
    elif image_count == 2:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-2")
    elif image_count == 3:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-3")
    elif image_count == 4:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-4")
    else:
        run_script_no_thread("restore/com/twitter/twitter.yaml", "image-count-0")


def run(tweet=None):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Retriving com/twitter/tweet data_source: tweet=%s", tweet)

    if tweet:
        get_tweet(int(tweet))


if __name__ == "__main__":
    get_tweet(1117187592850759680)
