import logging
import multiprocessing
import sched
import time

import requests
from bs4 import BeautifulSoup

from streamgraphicserver.util.data import send_data
from streamgraphicserver.util.logging import setup_loggers


def update_data(scheduler=None, test_mode=False):
    logger = logging.getLogger(__name__)

    session = requests.Session()
    session.trust_env = False

    if test_mode:
        logger.info("Requesting OSU emergency update. (TEST MODE ACTIVE)")
        r = session.get(
            "http://www.osu.edu/feeds/alert-test/feed.rss", timeout=(3.05, 27)
        )
    else:
        logger.info("Requesting OSU emergency update.")
        r = session.get(
            "http://www.osu.edu/feeds/emergency-alert.rss", timeout=(3.05, 27)
        )
    parser = BeautifulSoup(r.text, "lxml-xml")

    if parser.rss.item is not None:
        alert_raw = parser.rss.item.description.text
        alert_clean = BeautifulSoup(alert_raw, "lxml-xml").text
        alert_scroll = (
            "<marquee scrollamount='10' scrolldelay='60'>" + alert_clean + "</marquee>"
        )
        alert_color = "background: red; color: white;"
    else:
        alert_clean = ""
        alert_scroll = ""
        alert_color = "background: transparent; color: transparent;"

    logger.info('Updated OSU Emergency Text: "%s"', alert_clean)

    send_data("data/edu/osu/emergency/text", alert_clean)
    send_data("data/edu/osu/emergency/text-scroll", alert_scroll)
    send_data("data/edu/osu/emergency/background", alert_color)

    scheduler.enter(30, 1, update_data, argument=(scheduler, test_mode))


def run(test_mode=False):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting edu/osu/emergency data_source: test_mode=%s", test_mode)
    scheduler = sched.scheduler(time.time, time.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler, test_mode))
    scheduler.run()


if __name__ == "__main__":
    run(True)
