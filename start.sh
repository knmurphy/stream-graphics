#!/bin/sh

gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 streamgraphicserver.server:app -b 0.0.0.0:8000
