# OSU Buckeye Gaming Collective's Stream Graphics

Current Version: _v8.0.0_

## Requirements

- Python 3+
- Redis

A Docker file will be provided soon to make set up easier.  

## Compatible Browsers

- Chrome 57+
- Edge 16+
- Firefox 52+
- Opera 44+
- Safari 10.1+
- Mobile Safari 10.3+

## Known issues

- Text will be cut off using a '...' if the text is too long.
- **New skew option should not be used when a component is set to span more than one row.**

## Future plans

- Add graphical overlay designer/editor.

## Demo

![alt text](https://gitlab.com/enzanki_ars/stream-graphics/raw/master/demo.png)
